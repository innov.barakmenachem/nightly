#To create the image run: `docker build -t "first-flutter-try" .` 
#To run the container run: `docker run --rm -it first-flutter-try:latest`

# Setting the base image of which docker image is being created
FROM ubuntu:latest

# Switching to root user to install dependencies and flutter
USER root
# -------------------- 1 --------------------
# setup environment
RUN apt update && apt-get update && apt-get install -y --yes git wget unzip bash curl xz-utils libglu1-mesa \
    && apt-get clean

# -------------------- 2 --------------------
# Installing the different dependencies required for Flutter, installing flutter from beta channel from github and giving permissions to jenkins user to the folder
RUN git clone -b beta https://github.com/flutter/flutter.git /usr/local/flutter
# Running flutter doctor to check if flutter was installed correctly
RUN /usr/local/flutter/bin/flutter doctor -v \
    && rm -rfv /flutter/bin/cache/artifacts/gradle_wrapper


# -------------------- 4 --------------------
# Setting flutter and dart-sdk to PATH so they are accessible from terminal
ENV PATH="/usr/local/flutter/bin:/usr/local/flutter/bin/cache/dart-sdk/bin:${PATH}"

# -------------------- 5 - sill not work --------------------
#install android stutio 
#RUN apt install android-sdk


