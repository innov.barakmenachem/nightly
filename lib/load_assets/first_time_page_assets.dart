import 'package:flutter_svg/flutter_svg.dart';

SvgPicture svg_circles = new SvgPicture.asset(
    'assets/cir.svg',
    allowDrawingOutsideViewBox: true,
  );
  SvgPicture svg_logo = new SvgPicture.asset(
    'assets/logo.svg',
    allowDrawingOutsideViewBox: true,
  );
  SvgPicture svg_beer = new SvgPicture.asset(
    'assets/beer.svg',
    allowDrawingOutsideViewBox: true,
  );

  SvgPicture svg_nextbtn = new SvgPicture.asset(
    'assets/next.svg',
    allowDrawingOutsideViewBox: true,
  );