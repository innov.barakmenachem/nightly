import 'package:flutter/material.dart';
import 'package:nightly/custum_widgets/pin_code.dart';
import 'package:nightly/load_assets/first_time_page_assets.dart';

Widget logo = Padding(
  padding: EdgeInsets.only(top: 100, bottom: 20),
  child: svg_logo,
);

Widget pin_code_container = Container(
  padding: EdgeInsets.only(top: 20),
  child: Padding(
    padding: EdgeInsets.only(top: 10),
    child: Container(
      padding: EdgeInsets.only(top: 10),
      decoration: BoxDecoration(
        border: Border.all(
          color: Color(0xFF383838),
          width: 1.0,
        ),
        borderRadius: BorderRadius.all(
            Radius.circular(10.0) //         <--- border radius here
            ),
      ),
      child: PinCode(
        title: "Registration Code",
      ),
    ),
  ),
);
