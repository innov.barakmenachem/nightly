import 'package:flutter/material.dart';
import 'package:nightly/custum_widgets/scaffold_background_forground.dart';
import 'package:nightly/pages/first_time_page_widgets.dart';
import 'package:nightly/load_assets/first_time_page_assets.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'LevelUp/Nightly',
      theme: ThemeData(backgroundColor: Colors.white, fontFamily: 'Montserrat'),
      home: FirstTimeUsePage(),
    );
  }
}

class FirstTimeUsePage extends StatefulWidget {
  FirstTimeUsePage({Key key}) : super(key: key);

  @override
  _FirstTimeUsePageState createState() => _FirstTimeUsePageState();
}

class _FirstTimeUsePageState extends State<FirstTimeUsePage> {
  String currentText;
  @override
  Widget build(BuildContext context) {
    Widget forgroud = Container(
        padding: const EdgeInsets.symmetric(horizontal: 30),
        child: Center(
            child: Column(children: <Widget>[
          logo,
          pin_code_container,
          SizedBox(
            height: 200,
          ),
          Container(
              child: Column(
            children: <Widget>[
              Text(
                "Are you a club manager?",
                style: TextStyle(fontSize: 16),
                textAlign: TextAlign.left,
              ),
              RaisedButton(
                padding: EdgeInsets.symmetric(horizontal: 40),
                shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(50.0),
                ),
                onPressed: () {},
                color: Color.fromARGB((255*0.4).round(), 1, 1, 1),
                textColor: Colors.white,
                child: Text("Open Your Club >",
                    style: TextStyle(fontSize: 14)),
              ),
            ],
          ))
        ])));

    Widget background = Center(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[svg_beer, svg_circles],
      ),
    );

    return ScaffoldBackgroundForground(
      child_back: background,
      child_front: forgroud,
    );
  }
}
