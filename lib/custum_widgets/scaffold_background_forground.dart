import 'package:flutter/material.dart';

class ScaffoldBackgroundForground extends StatefulWidget {
  ScaffoldBackgroundForground({Key key, @required this.child_back,@required this.child_front})
      : super(key: key);
  final Widget child_back;
  final Widget child_front;

  @override
  _ScaffoldBackgroundForgroundState createState() =>
      _ScaffoldBackgroundForgroundState();
}

class _ScaffoldBackgroundForgroundState
    extends State<ScaffoldBackgroundForground> {
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key:  scaffoldKey,
      resizeToAvoidBottomPadding: false,
      resizeToAvoidBottomInset: false,
      backgroundColor: Theme.of(context).backgroundColor,
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: Stack(
            children: <Widget>[widget.child_back, widget.child_front],
          ),
        ),
      ),
    );
    
  }
}
