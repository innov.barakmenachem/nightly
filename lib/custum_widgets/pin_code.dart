import 'package:flutter/material.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

class PinCode extends StatefulWidget {
  PinCode({
    Key key,
    @required this.title,
  }) : super(key: key);
  String title = "";

  @override
  _PinCodeState createState() => _PinCodeState();
}

class _PinCodeState extends State<PinCode> {
  String currentText = "";

  @override
  Widget build(BuildContext context) {
    PinCodeTextField pincode = PinCodeTextField(
      length: 5,
      obsecureText: false,
      backgroundColor: Colors.transparent,
      inactiveColor: Colors.blueGrey,
      animationType: AnimationType.slide,
      shape: PinCodeFieldShape.underline,
      animationDuration: Duration(milliseconds: 200),
      borderRadius: BorderRadius.circular(1),
      onChanged: (value) {
        setState(() {
          currentText = value;
        });
      },
    );

    return Container(
      child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding:
                  const EdgeInsets.symmetric(vertical: 0, horizontal: 15),
              child: Text(
                widget.title,
                style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
                textAlign: TextAlign.left,
              ),
            ),
            Padding(
              padding: EdgeInsets.only(
                left: 40,
                right: 40,
                bottom: 10,
              ),
              child: pincode,
            ),
          ]),
    );
  }
}
